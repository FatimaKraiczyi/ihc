import React, { useState } from 'react';
import styles from '../Home/Home.module.sass'
import { Chip, Box, Typography, Button } from '@mui/material';
import { ArrowBackIos, ArrowForwardIos } from '@mui/icons-material';
import TGP from '../../Assets/TGP.svg';

const Home = () => {
    const [activeSlide, setActiveSlide] = useState(0);

    const handleSlideChange = (index) => {
        setActiveSlide(index);
    };

    const handleNextSlide = () => {
        setActiveSlide((prevSlide) => (prevSlide + 1) % slides.length);
    };

    const handlePrevSlide = () => {
        setActiveSlide((prevSlide) => (prevSlide - 1 + slides.length) % slides.length);
    };

    const slides = [
        {
            image: TGP,
            title: 'teste',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            buttonText: 'Lista de cursos'
        },
        {
            image: TGP,
            title: 'teste',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            buttonText: 'Lista de cursos'
        },
        {
            image: TGP,
            title: 'teste',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            buttonText: 'Lista de cursos'
        },
    ];

    return (
        <div className={styles.container}>
            <Chip label="Cursos Ofertados" className={styles.customChip} />

            {/* Carrossel */}
            <Box
                display="flex"
                flexDirection="column"
                alignItems="center"
                mt={4}
                p={2}
                bgcolor="#F5F5F5"
                borderRadius={8}
                boxShadow={1}
            >
                {/* Slides */}
                <Box display="flex" justifyContent="center" width="100%">
                    {/* Slide Ativo */}
                    <Box
                        position="relative"
                        display="flex"
                        alignItems="center"
                        maxWidth="800px"
                        p={2}
                    >
                        <ArrowBackIos
                            className={styles.carouselArrow}
                            onClick={handlePrevSlide}
                            style={{ left: '0' }}
                        />
                        <img src={slides[activeSlide].image} alt={`Imagem ${activeSlide + 1}`} />
                        <Typography variant="body1">{slides[activeSlide].text}</Typography>
                        <Button variant="contained" color="primary">
                            {slides[activeSlide].buttonText}
                        </Button>
                        <ArrowForwardIos
                            className={styles.carouselArrow}
                            onClick={handleNextSlide}
                            style={{ right: '0' }}
                        />
                    </Box>
                </Box>

                {/* Bolinhas inferiores */}
                <Box display="flex" justifyContent="center" mt={2}>
                    {slides.map((_, index) => (
                        <Box
                            key={index}
                            width="10px"
                            height="10px"
                            borderRadius="50%"
                            bgcolor={index === activeSlide ? 'blue' : 'gray'}
                            mx={0.5}
                            onClick={() => handleSlideChange(index)}
                            style={{ cursor: 'pointer' }}
                        />
                    ))}
                </Box>
            </Box>
        </div>
    );
};

export default Home;
