import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './Components/Header/Header';
import Home from '../src/Components/Home/Home';
import Aluno from '../src/Components/Aluno/Aluno';
import Concursos from '../src/Components/Concursos/Concursos';
import Informacao from '../src/Components/Informacao/Informacao';
import SaibaMais from '../src/Components/SaibaMais/SaibaMais';
import Contato from '../src/Components/Contato/Contato';
import './App.scss';
import Footer from './Components/Footer/Footer';

const App = () => {
  return (
    <Router>
      <div>
        <Header />
        <main className="content">
          <div className="contentWrapper">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/areadoaluno" element={<Aluno />} />
              <Route path="/concursosetestes" element={<Concursos />} />
              <Route path="/acessoainformacao" element={<Informacao />} />
              <Route path="/saibamais" element={<SaibaMais />} />
              <Route path="/contato" element={<Contato />} />
            </Routes>
          </div>
        </main>
        <Footer />
      </div>
    </Router>
  );
};

export default App;