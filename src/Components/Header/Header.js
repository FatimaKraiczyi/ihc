import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Tab, Tabs } from '@mui/material';
import header from '../../Assets/header.svg';
import styles from './Header.module.sass';

const Header = () => {
    const [activeTab, setActiveTab] = useState(0);

    const handleTabChange = (event, newValue) => {
        setActiveTab(newValue);
    };

    return (
        <header className={`${styles.header} ${styles.fixed}`}>
            <img src={header} alt="UNIVERSIDADE" />
            <nav>
                <Tabs
                    value={activeTab}
                    onChange={handleTabChange}
                    textColor="primary"
                    indicatorColor="primary"
                >
                    <Tab
                        label="Home"
                        component={Link}
                        to="/"
                        classes={{ selected: styles.selected }}
                        className={styles.tab}
                    />
                    <Tab
                        label="Área do aluno"
                        component={Link}
                        to="/areadoaluno"
                        classes={{ selected: styles.selected }}
                        className={styles.tab}
                    />
                    <Tab
                        label="Concursos e testes"
                        component={Link}
                        to="/concursosetestes"
                        classes={{ selected: styles.selected }}
                        className={styles.tab}

                    />
                    <Tab
                        label="Acesso a informação"
                        component={Link}
                        to="/acessoainformacao"
                        classes={{ selected: styles.selected }}
                        className={styles.tab}
                    />
                    <Tab
                        label="Saiba mais"
                        component={Link}
                        to="/saibamais"
                        classes={{ selected: styles.selected }}
                        className={styles.tab}
                    />
                    <Tab
                        label="Contato"
                        component={Link}
                        to="/contato"
                        classes={{ selected: styles.selected }}
                        className={styles.tab}
                    />
                </Tabs>
            </nav>
        </header>
    );
};

export default Header;
