import React from 'react';
import facebookIcon from '../../Assets/facebookIcon.png';
import twitterIcon from '../../Assets/twitterIcon.png';
import youtubeIcon from '../../Assets/youtubeIcon.png';
import universityLogo from '../../Assets/universityLogo.png';
import rodape from '../../Assets/rodape.svg';
import styles from './Footer.module.sass';

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles.content}>
                <img src={rodape} alt="" />

                {/*          <div>
                    <p>Redes Sociais</p>
                    <div className={styles.socialMedia}>
                        <img src={facebookIcon} alt="Facebook" />
                        <img src={twitterIcon} alt="Twitter" />
                        <img src={youtubeIcon} alt="YouTube" />
                    </div>
                </div>
                <div className={styles.universityInfo}>
                    <div className={styles.info}>
                        <h4>Universidade Federal do Paraná</h4>
                        <h4>SEPT</h4>
                        <p>Rua Dr. Alcides Vieira Arcoverde, 1225</p>
                        <p>Jardim das Américas, Curitiba - PR, 81520-260</p>
                        <p>Telefone: (41) 3361-4905</p>
                    </div>
                    <div className={styles.logo}>
                        <img src={universityLogo} alt="University Logo" />
                    </div>
                </div> */}
            </div>
            {/* <div className={styles.copyright}>
                &copy; {new Date().getFullYear()} - Universidade Federal do Paraná - SEPT
                <p> Desenvolvido pela AGTIC - Agência de Tecnologia da Informação e Comunicação da UFPR</p>
            </div> */}
        </footer>
    );
};

export default Footer;
